package fr.lacombedulionvert.drinks;

public class Coffee implements Drink {
    @Override
    public void serve() {
        System.out.print("Serving coffee");
    }
}
