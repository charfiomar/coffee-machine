package fr.lacombedulionvert.drinks;

public class Tea implements Drink {
    @Override
    public void serve() {
        System.out.print("Serving tea");
    }
}
