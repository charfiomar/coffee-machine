package fr.lacombedulionvert.drinks;

public class Chocolate implements Drink {
    @Override
    public void serve() {
        System.out.print("Serving chocolate");
    }
}
