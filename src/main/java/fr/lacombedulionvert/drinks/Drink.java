package fr.lacombedulionvert.drinks;

public interface Drink {

    void serve();
}
