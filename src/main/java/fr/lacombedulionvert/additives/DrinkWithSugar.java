package fr.lacombedulionvert.additives;

import fr.lacombedulionvert.additives.DrinkAdditive;
import fr.lacombedulionvert.drinks.Drink;

public class DrinkWithSugar extends DrinkAdditive {

    private int sugar;

    public DrinkWithSugar(Drink drink, int sugar) {
        super(drink);
        this.sugar = Math.max(sugar, 1);
    }

    public int getSugar() {
        return sugar;
    }

    public void setSugar(int sugar) {
        this.sugar = sugar;
    }

    @Override
    public void serve() {
        super.serve();
        System.out.print(" with " + sugar + " sugar and 1 stick.");
    }
}
