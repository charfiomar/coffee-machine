package fr.lacombedulionvert.additives;

import fr.lacombedulionvert.drinks.Drink;

public class DrinkAdditive implements Drink {

    protected Drink drink;

    public DrinkAdditive(Drink drink){
        this.drink = drink;
    }

    @Override
    public void serve() {
        this.drink.serve();
    }
}
