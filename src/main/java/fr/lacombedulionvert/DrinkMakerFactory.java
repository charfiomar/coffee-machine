package fr.lacombedulionvert;

import fr.lacombedulionvert.additives.DrinkWithSugar;
import fr.lacombedulionvert.drinks.Chocolate;
import fr.lacombedulionvert.drinks.Coffee;
import fr.lacombedulionvert.drinks.Drink;
import fr.lacombedulionvert.drinks.Tea;
import fr.lacombedulionvert.exceptions.InvalidDrinkMakerInputException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DrinkMakerFactory {

    private final List<String> acceptedCommands = Arrays.asList("T", "H", "C", "M");

    public Drink getDrink(String drinkMakerCommand) {

        String[] command = drinkMakerCommand.split(":");
        String drinkType = command[0];

        if (command.length == 0 || command.length > 3 || !acceptedCommands.contains(drinkType))
            throw new InvalidDrinkMakerInputException();

        Drink base;

        switch (drinkType) {
            case "T": {
                base = new Tea();
                break;
            }
            case "H": {
                base = new Chocolate();
                break;
            }
            case "C": {
                base = new Coffee();
                break;
            }
            case "M": {
                System.out.print(command[1]);
                return null;
            }
            default: {
                throw new InvalidDrinkMakerInputException();
            }
        }

        if (command.length == 3) {
            try {
                int sugar = Integer.parseInt(command[1]);
                if (sugar <= 0) throw new InvalidDrinkMakerInputException();
                return new DrinkWithSugar(base, sugar);
            } catch (NumberFormatException | InvalidDrinkMakerInputException e) {
                throw new InvalidDrinkMakerInputException();
            }
        }

        return base;
    }
}
