package fr.lacombedulionvert;

import fr.lacombedulionvert.drinks.Chocolate;
import fr.lacombedulionvert.drinks.Drink;
import fr.lacombedulionvert.additives.DrinkWithSugar;

public class CoffeeMachine {

    public static void main(String[] args) {
        DrinkMakerFactory factory = new DrinkMakerFactory();

        Drink drink = factory.getDrink("T:1:4");

        if(drink != null) drink.serve();
    }
}
