package fr.lacombedulionvert.exceptions;

public class InvalidDrinkMakerInputException extends RuntimeException{

    @Override
    public String getMessage() {
        return "Invalid drink maker input.";
    }
}
