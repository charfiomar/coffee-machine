package fr.lacombedulionvert;

import fr.lacombedulionvert.drinks.Drink;
import fr.lacombedulionvert.exceptions.InvalidDrinkMakerInputException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class DrinkMakerInputTest {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    private final DrinkMakerFactory factory = new DrinkMakerFactory();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void testTeaWith1Sugar(){
        Drink drink = factory.getDrink("T:1:0");
        assertNotNull(drink);
        drink.serve();
        assertEquals("Serving tea with 1 sugar and 1 stick.", outContent.toString());
    }

    @Test
    public void testChocolate(){
        Drink drink = factory.getDrink("H::");
        assertNotNull(drink);
        drink.serve();
        assertEquals("Serving chocolate", outContent.toString());
    }

    @Test
    public void testCoffeeWith2Sugar(){
        Drink drink = factory.getDrink("C:2:0");
        assertNotNull(drink);
        drink.serve();
        assertEquals("Serving coffee with 2 sugar and 1 stick.", outContent.toString());
    }

    @Test
    public void testCustomMessage(){
        Drink drink = factory.getDrink("M:CustomMessage");
        assertNull(drink);
        assertEquals("CustomMessage", outContent.toString());
    }

    @Test
    public void testInvalidDrinkMakerInputExceptionThrown(){
        Exception exception = assertThrows(InvalidDrinkMakerInputException.class, () -> {
            Drink drink = factory.getDrink("B:1:0");
            assertNull(drink);
        });

        String expectedMessage = "Invalid drink maker input.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }



    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
}
